-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (native)
Source: video-dl
Binary: video-dl
Architecture: all
Version: 3.4.242
Maintainer: Daniil Gentili <daniil.gentili.dg@gmail.com>
Homepage: http://daniil.it/video-dl
Standards-Version: 3.9.4
Build-Depends: debhelper (>= 8.0.0)
Package-List: 
 video-dl deb misc optional
Checksums-Sha1: 
 077cded612dbcf2a149ef54cce129800f963be01 19300 video-dl_3.4.242.tar.gz
Checksums-Sha256: 
 c86f4d69e85ea97d12f6366bc80f775cfaa524eae0336ce67fc0d9b00c401dce 19300 video-dl_3.4.242.tar.gz
Files: 
 e258109cebe694a95b8208d71b10dea7 19300 video-dl_3.4.242.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.11 (GNU/Linux)

iQIcBAEBAgAGBQJWhn7ZAAoJEHK5f9HZZyyTiNoQAJSZpL0vOTA6fxfZmEppFtGh
o+OZd+FXEarFODDTr4eUsQ92/qs2KOD4AMUFUoRtizHYyu0hskT0OmaTNXlPrBI+
EWhYizi4zhetUf7o4XiZMLYzVCTxubvFyfF8tAM+D3Ku9WH4+M7cVEGhwPhZWM1j
LVHpCzxrxaA//fp8BYxhxZTrmrUn63JhYzlmYX+OBFZtDHspO4lrLvZ3u807mF8L
YL3Lt6/sMvlB1mceDnVrYZIdysU4pL4LTJRUX9X2Iype8dmjPVScUN15QwYYAHaw
SLhuJe1BDls8uYlkMI+vXZmzrC9jSrF4ORCiN5ZFKhNqwmfHTkKOdeA6dghkACeO
7dJxB73FoHWDybXOLYs/1pujp2ZGBBAH0zjkqPKoULwfJvpFVACfF9AhewZ4WA6a
qDhG/bpmHlwQkw4ehdTokc4XLfNNWo6xcUy9Rx1ZdLta9ImTSXoTbvcU3KDnrqB5
73ZAfhyO2L+2Tv3UThu2j3mJ1M44TKJ8pQUx5Lx2roB2hzRIyNeUa9TACu8lqTtw
4Wa1zmKQJQUORxeeu1Dp85mhqpUIN5OHPzzefU9vgLXz6Kp8YOW2GXYBROuTDv31
mnepOBRLB8AMffii1cXvvun47YiWj4XZz17H5XNlFMf+4kzuiGwCAivs84FwPBHH
zo2BtMFFagmWzTO0un2b
=zB+z
-----END PGP SIGNATURE-----
